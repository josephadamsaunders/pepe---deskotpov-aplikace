package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Dialog;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BlacknWhiteDialog extends Dialog {


    @FXML
    private ImageView bigImage;

    @FXML
    private AnchorPane dialogAnchor;

    private BufferedImage originalImage;

    private BufferedImage modifiedImage;

    @FXML
    private Slider slider;
    private int sliderPosition;

    @FXML
    public javafx.scene.control.Label value;

    public javafx.scene.control.CheckBox checkBoxAuto;

    public BlacknWhiteDialog() {
    }

    public void setBigImage(Image image) {
        bigImage.setImage(image);
        originalImage = SwingFXUtils.fromFXImage(image, null);
        modifiedImage = originalImage;
        slider.setMin(0);
        slider.setMax(256);
        slider.setValue(128);
        checkBoxAuto.setSelected(false);
        slider.valueProperty().addListener((ov, old_val, new_val) -> {
            value.setText("Threshold Value:" + String.format("%.2f", new_val));
            sliderPosition = new_val.intValue();
            applyFilter();
        });
    }


    @FXML
    private void automaticThreshold(){
        if (!slider.isDisable()) {
            long outInt = 0;
            for (int x = 0; x < originalImage.getWidth(); x++) {
                for (int y = 0; y < originalImage.getHeight(); y++) {
                    Color c = new Color(originalImage.getRGB(x, y));
                    outInt = outInt + c.getRed() + c.getGreen() + c.getBlue();
                }
            }
            outInt = outInt / (originalImage.getHeight() * originalImage.getWidth() * 3);
            slider.setValue((int) outInt);
            slider.setDisable(true);
        } else {
            slider.setDisable(false);
        }
    }

    @FXML
    private void applyFilter(){
        int black = Color.BLACK.getRGB();
        int white = Color.WHITE.getRGB();
        modifiedImage = new BufferedImage(originalImage.getWidth(),originalImage.getHeight(),originalImage.getType());
        for (int x = 0; x < originalImage.getWidth(); x++){
            for (int y = 0; y < originalImage.getHeight(); y++){
                int rgb = originalImage.getRGB(x, y);
                Color c = new Color(rgb);
                if (c.getRed() > sliderPosition ||
                        c.getGreen() > sliderPosition ||
                        c.getBlue() > sliderPosition){
                    modifiedImage.setRGB(x, y, white);
                } else {
                    modifiedImage.setRGB(x,y,black);
                }
            }
        }
        Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        bigImage.setImage(newImage);
    }

    @FXML
    public void exit(ActionEvent event) {
        Stage stage = (Stage) dialogAnchor.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void apply() {
        Controller.modifiedImage = this.modifiedImage;
        Stage stage = (Stage) dialogAnchor.getScene().getWindow();
        stage.close();
        Controller.hasBeenModified = true;
    }

}
