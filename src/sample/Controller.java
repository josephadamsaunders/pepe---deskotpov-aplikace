package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Controller {

    @FXML
    private AnchorPane mainAnchorPane;

    @FXML
    private ImageView bigImage;

    @FXML
    private RadioButton originalImageRadioButton;

    public static boolean hasBeenModified = false;

    @FXML
    private RadioButton modifiedImageRadioButton;

    @FXML
    private Button selectImageFileButton;

    @FXML
    private Button editMatrixButton;

    @FXML
    private Button applyMatrixFilterButton;

    @FXML
    private Button generateImageButton;

    @FXML
    private Button restoreOriginalImageButton;

    @FXML
    private TextArea textArea;

    @FXML
    private ImageView smallImage;

    //menu Items

    @FXML
    private javafx.scene.control.MenuItem menuFiltersNegative;
    @FXML
    private javafx.scene.control.MenuItem menuFiltersDelete;
    @FXML
    private javafx.scene.control.MenuItem menuFiltersBlackNWhite;
    @FXML
    private javafx.scene.control.MenuItem menuFiltersPixelate;
    @FXML
    private MenuItem menuFileSave;


    public BufferedImage originalImage;

    public static BufferedImage modifiedImage;

    @FXML
    public void initialize(){
        disableButtons();
    }

    @FXML
    public void exit(ActionEvent event) {
        Stage stage = (Stage) mainAnchorPane.getScene().getWindow();
        stage.close();
    }

    @FXML
    void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("About");
        alert.setContentText("Aplikace pro pepeho :)");
        alert.show();
    }

    @FXML
    void loadImage(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("*.jpg", "*.JPG"),
                new FileChooser.ExtensionFilter("*.png", "*.PNG")
        );
        File file = fileChooser.showOpenDialog(null);
        try {
            originalImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(originalImage, null);
            bigImage.setImage(image);
            modifiedImage = originalImage;
        } catch (Exception ignored) {
            System.out.println("no image loaded");
        }
        allowButtons();
    }


    @FXML
    private void saveImage(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save image...");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("*.jpg", "*.JPG"),
                new FileChooser.ExtensionFilter("*.png", "*.PNG")
        );
        File file = fileChooser.showSaveDialog(null);
        try {
            ImageIO.write(modifiedImage, "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void generateImage(ActionEvent event) {
        makeColouredImage();
        Image newImage = SwingFXUtils.toFXImage(originalImage, null);
        bigImage.setImage(newImage);
        allowButtons();
        modifiedImage = originalImage;
    }

    private void makeColouredImage(){
        originalImage = new BufferedImage(600, 600, BufferedImage.TYPE_3BYTE_BGR);
        int rgb;
        for (int x = 1; x < originalImage.getWidth()+1; x++){
            rgb = 0;
            for (int y = 1; y < originalImage.getHeight()+1; y++){
                originalImage.setRGB(x-1, y-1, (new Color(255-rgb%x,255-rgb,255-rgb).getRGB()));
                rgb+=1;
                if (rgb == 255) {
                    rgb = 0;
                }
            }
        }
    }


    @FXML
    private void selectOriginalImageRadioButton(ActionEvent event){
        originalImageRadioButton.setSelected(true);
        modifiedImageRadioButton.setSelected(false);
        Image newImage = SwingFXUtils.toFXImage(originalImage, null);
        bigImage.setImage(newImage);
    }

    @FXML
    private void selectModifiedImageRadioButton(ActionEvent event){
        originalImageRadioButton.setSelected(false);
        modifiedImageRadioButton.setSelected(true);
        Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        bigImage.setImage(newImage);
    }

    @FXML
    public void negativeFilter(ActionEvent event){
        BufferedImage negativeImage = new BufferedImage(modifiedImage.getWidth(),
                modifiedImage.getHeight(), modifiedImage.getType());
        for (int x = 0; x < originalImage.getWidth(); x++){
            for (int y = 0; y < originalImage.getHeight(); y++){
                int rgbOrig = modifiedImage.getRGB(x, y);
                Color c = new Color(rgbOrig);
                int r = 255 - c.getRed();
                int g = 255 - c.getGreen();
                int b = 255 - c.getBlue();
                Color nc = new Color(r,g,b);
                negativeImage.setRGB(x,y,nc.getRGB());
            }
        }
        modifiedImage = negativeImage;

        Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        bigImage.setImage(newImage);
        hasBeenModified = true;
        switchRadio();
    }

    private void allowButtons(){
        originalImageRadioButton.setDisable(false);
        modifiedImageRadioButton.setDisable(false);
        originalImageRadioButton.setSelected(true);
        modifiedImageRadioButton.setSelected(false);
        menuFiltersNegative.setDisable(false);
        menuFiltersDelete.setDisable(false);
        menuFileSave.setDisable(false);
        menuFiltersBlackNWhite.setDisable(false);
        menuFiltersPixelate.setDisable(false);
        //restoreOriginalImageButton.setDisable(false);
        //applyMatrixFilterButton.setDisable(false);
        //editMatrixButton.setDisable(false);
    }

    private void disableButtons(){
        originalImageRadioButton.setDisable(true);
        modifiedImageRadioButton.setDisable(true);
        originalImageRadioButton.setSelected(false);
        modifiedImageRadioButton.setSelected(false);
        menuFiltersNegative.setDisable(true);
        menuFiltersDelete.setDisable(true);
        menuFileSave.setDisable(true);
        restoreOriginalImageButton.setDisable(true);
        applyMatrixFilterButton.setDisable(true);
        editMatrixButton.setDisable(true);
        menuFiltersBlackNWhite.setDisable(true);
        menuFiltersPixelate.setDisable(true);
    }

    @FXML
    public void delete(ActionEvent event) {
        disableButtons();
        originalImage = null;
        bigImage.setImage(null);
    }
    
    @FXML
    public void blackNWhiteFilter(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BlacknWhiteDialog.fxml"));
            Parent parent = fxmlLoader.load();
            BlacknWhiteDialog dialogController = fxmlLoader.getController();
            Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
            dialogController.setBigImage(newImage);

            Scene scene = new Scene(parent, 720, 600);
            Stage stage = new Stage();
            stage.setMinHeight(600);
            stage.setMinWidth(720);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            newImage = SwingFXUtils.toFXImage(modifiedImage, null);
            bigImage.setImage(newImage);
            if (hasBeenModified) {
                switchRadio();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void pixelFilter() {try {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PixelateDialog.fxml"));
        Parent parent = fxmlLoader.load();
        PixelateDialog dialogController = fxmlLoader.getController();
        Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        dialogController.setBigImage(newImage);

        Scene scene = new Scene(parent, 720, 600);
        Stage stage = new Stage();
        stage.setMinHeight(600);
        stage.setMinWidth(720);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
        newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        bigImage.setImage(newImage);
        if (hasBeenModified) {
            switchRadio();
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
    }

    public void switchRadio(){
        if (hasBeenModified) {
            originalImageRadioButton.setSelected(false);
            modifiedImageRadioButton.setSelected(true);
        } else {
            originalImageRadioButton.setSelected(true);
            modifiedImageRadioButton.setSelected(false);
        }
    }
}