package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Dialog;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class PixelateDialog  extends Dialog {

    public PixelateDialog() {
    }

    @FXML
    private ImageView bigImage;

    @FXML
    private AnchorPane dialogAnchor;

    private BufferedImage originalImage;

    private BufferedImage modifiedImage;

    @FXML
    private Slider slider;
    private int factor;

    @FXML
    public javafx.scene.control.Label value;

    public void setBigImage(Image image) {
        bigImage.setImage(image);
        originalImage = SwingFXUtils.fromFXImage(image, null);
        modifiedImage = originalImage;
        slider.setMin(1);
        slider.setMax(15);
        slider.setValue(1);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(1);
        slider.setSnapToTicks(true);
        slider.valueProperty().addListener((ov, old_val, new_val) -> {
            value.setText("Factor Value:" + String.format("%.2f", new_val));
            factor = new_val.intValue();
            applyFilter();
        });
    }

    @FXML
    private void applyFilter(){
        BufferedImage pixelatedImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(),
                originalImage.getType());
        for (int x = 0; x < originalImage.getWidth();x = x + factor * 2){
            for (int y = 0; y < originalImage.getHeight();y = y + factor * 2){
                for (int a = x - factor; a < x + factor; a++){
                    for (int b = y - factor; b < y + factor; b++){
                        int getA = Math.min(originalImage.getWidth()-1, Math.max(0, a));
                        int getB = Math.min(originalImage.getHeight()-1, Math.max(0, b));
                        Color c = new Color(originalImage.getRGB(x, y));
                        pixelatedImage.setRGB(getA, getB, c.getRGB());
                    }
                }
            }
        }
        modifiedImage = pixelatedImage ;
        Image newImage = SwingFXUtils.toFXImage(modifiedImage, null);
        bigImage.setImage(newImage);
    }

    @FXML
    public void exit(ActionEvent event) {
        Stage stage = (Stage) dialogAnchor.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void apply() {
        Controller.modifiedImage = this.modifiedImage;
        Stage stage = (Stage) dialogAnchor.getScene().getWindow();
        stage.close();
        Controller.hasBeenModified = true;
    }

}
